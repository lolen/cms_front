import React from 'react';

export default class Footer extends React.Component {
    render() {
        return (
            <footer className="py-5 bg-primary">
                <div className="container">
                    <p className="m-0 text-center text-white">Copyright &copy; Your Website 2017</p>
                </div>
            </footer>
        )
    }
}