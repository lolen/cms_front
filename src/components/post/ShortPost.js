import React from 'react';
import HumanTime from 'react-human-time'
import {Link} from 'react-router-dom'

export default class ShortPost extends React.Component {
    render() {
        return (
            <div className="card mb-4">
                <img className="card-img-top" src="http://placehold.it/750x300" alt={this.props.post.title}/>
                <div className="card-body">
                    <h2 className="card-title">{this.props.post.title}</h2>
                    <p className="card-text">
                        {this.props.post.summary}
                    </p>
                    <Link to={"/posts/show/" + this.props.post.id} className="btn btn-primary">Read More &rarr;</Link>
                </div>
                <div className="card-footer text-muted">
                    <i className="fas fa-clock"></i> <HumanTime time={this.props.post.published_at}/> |
                    {!this.props.post.tags.length ? null :
                        (<span> <i className="fas fa-tags"></i>
                            {this.props.post.tags.map(function (tag) {
                                return (
                                    <span key={tag.id}> <Link to="/" className="tags">
                                        <span className="badge badge-primary">Bootstrap</span></Link>
                                    </span>
                                );
                            })}
                        </span>)
                    }
                </div>
            </div>
        )
    }
}