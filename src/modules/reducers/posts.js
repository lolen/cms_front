export const POSTS_FETCHING = 'posts/fetching';
export const POSTS_FETCH_FAIL = 'posts/fetch_fail';
export const POSTS_FETCH_SUCCESS = 'posts/fetch_success';
export const POSTS_CLEAR = 'posts/clear';

const initialState = {
    isLoading: false,
    isError: false,
    data: {}
};

export default (state = initialState, action) => {
    switch (action.type) {
        case POSTS_FETCHING:
            return {...state, isLoading: true, isError: false};
        case POSTS_FETCH_FAIL:
            return {...state, isLoading: false, isError: true};
        case POSTS_FETCH_SUCCESS:
            return {...state, isLoading: false, isError: false, data: action.data };
        case POSTS_CLEAR:
            return initialState;
        default:
            return state
    }
}