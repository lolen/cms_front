export const POST_FETCHING = 'post/fetching';
export const POST_FETCH_FAIL = 'post/fetch_fail';
export const POST_FETCH_SUCCESS = 'post/fetch_success';
export const POST_CLEAR = 'post/clear';

const initialState = {
    isLoading: false,
    isError: false,
    data: {}
};

export default (state = initialState, action) => {
    switch (action.type) {
        case POST_FETCHING:
            return {...state, isLoading: true, isError: false};
        case POST_FETCH_FAIL:
            return {...state, isLoading: false, isError: true};
        case POST_FETCH_SUCCESS:
            return {...state, isLoading: false, isError: false, data: action.data };
        case POST_CLEAR:
            return initialState;
        default:
            return state
    }
}