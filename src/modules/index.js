import {combineReducers} from 'redux'
import {routerReducer} from 'react-router-redux'
import posts from './reducers/posts'
import post from './reducers/post'

export default combineReducers({
    router: routerReducer,
    posts,
    post
})
