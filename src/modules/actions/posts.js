import axios from 'axios';
import {POSTS_FETCHING, POSTS_FETCH_SUCCESS, POSTS_FETCH_FAIL, POSTS_CLEAR} from '../reducers/posts'

export function getPosts() {
    return dispatch => {
        dispatch({type: POSTS_FETCHING});

        axios.get('http://api.lab.lolen.pl/posts')
            .then(function (response) {
                dispatch({type: POSTS_FETCH_SUCCESS, data: response.data});
            })
            .catch(function (error) {
                dispatch({type: POSTS_FETCH_FAIL});
            })
    }
}

export function clearPosts() {
    return dispatch => {
        dispatch({type: POSTS_CLEAR});
    }
}