import axios from 'axios';
import {POST_FETCHING, POST_FETCH_SUCCESS, POST_FETCH_FAIL, POST_CLEAR} from '../reducers/post'

export function getPost(id) {
    return dispatch => {
        dispatch({type: POST_FETCHING});

        axios.get('http://api.lab.lolen.pl/posts/show/'+id)
            .then(function (response) {
                dispatch({type: POST_FETCH_SUCCESS, data: response.data});
            })
            .catch(function (error) {
                dispatch({type: POST_FETCH_FAIL});
            })
    }
}

export function clearPost() {
    return dispatch => {
        dispatch({type: POST_CLEAR});
    }
}