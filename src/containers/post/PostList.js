import React from 'react';
import { connect } from 'react-redux'

import ShortPost from '../../components/post/ShortPost'

import { getPosts, clearPosts } from '../../modules/actions/posts'

const mapStateToProps = state => ({
    posts: state.posts
});

class PostList extends React.Component {
    componentWillMount() {
        this.props.dispatch(getPosts());
    }

    componentWillUnmount() {
        this.props.dispatch(clearPosts());
    }

    render() {
        if (this.props.posts.isLoading) {
            return (
                <div className="loader">Ładowanie</div>
            );
        }
        if (this.props.posts.isError || !Object.keys(this.props.posts.data).length) {
            return (
                <div>Bład pobierania danych</div>
            );
        }

        return (
            <div>
                {this.props.posts.data.posts.map(function (post, key) {
                    return (
                        <ShortPost post={post} key={key}/>
                    )
                })}
            </div>
        )
    }
}

export default connect(mapStateToProps)(PostList);
