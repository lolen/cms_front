import React from 'react';
import {connect} from "react-redux";
import {clearPost, getPost} from "../../modules/actions/post";
import HumanTime from 'react-human-time'
import {Link} from 'react-router-dom'

const mapStateToProps = state => ({
    post: state.post
});

class PostShow extends React.Component {
    componentWillMount() {
        this.props.dispatch(getPost(this.props.match.params.id));
    }

    componentWillUnmount() {
        this.props.dispatch(clearPost());
    }

    render() {
        if (this.props.post.isLoading) {
            return (
                <div className="loader">Ładowanie</div>
            );
        }
        if (this.props.post.isError || !Object.keys(this.props.post.data).length) {
            return (
                <div>Bład pobierania danych</div>
            );
        }

        return (
            <div className="card mb-4">
                <img className="card-img-top" src="http://placehold.it/750x300" alt={this.props.post.title}/>
                <div className="card-body">
                    <h2 className="card-title">{this.props.post.data.title}</h2>
                    <p className="card-text">
                        {this.props.post.data.contents}
                    </p>
                </div>
                <div className="card-footer text-muted">
                    <i className="fas fa-clock"></i> <HumanTime time={this.props.post.data.published_at}/> |
                    {!this.props.post.data.tags.length ? null :
                        (<span> <i className="fas fa-tags"></i>
                            {this.props.post.data.tags.map(function (tag) {
                                return (
                                    <span key={tag.id}> <Link to="/" className="tags">
                                        <span className="badge badge-primary">Bootstrap</span></Link>
                                    </span>
                                );
                            })}
                        </span>)
                    }
                </div>
            </div>
        )
    }
}
export default connect(mapStateToProps)(PostShow);