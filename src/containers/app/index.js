import React from 'react';
import {Route} from 'react-router-dom'


import Header from '../../components/app/Header'
import Footer from '../../components/app/Footer'

import About from '../about'

import PostList from "../post/PostList";
import PostShow from "../post/PostShow";

export default class App extends React.Component {
    render() {
        return (
            <div>
                <Header/>
                <div className="container">
                    <div className="row">
                        <div className="col-md-8">
                            <Route exact path="/" component={PostList}/>
                            <Route exact path="/posts/:page" component={PostList}/>
                            <Route exact path="/posts/show/:id" component={PostShow}/>
                            <Route exact path="/about-us" component={About}/>
                        </div>
                        <div className="col-md-4">
                            sada
                        </div>
                    </div>
                </div>
                <Footer/>
            </div>
        )
    }
}